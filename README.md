# Angular 16 Demo Application
This demo application showcases how to use Angular 16 signals and components standalone. The application provides several examples that demonstrate the power and flexibility of Angular 16.

## Prerequisites
Before running this demo application, you need to have the following software installed:

* `Node.js` (version 12.0 or higher)
* `Angular CLI` (version 16.0 or higher)

## Installation
Clone this repository.
Run `npm install` to install the dependencies.

## Usage
To start the demo application, run `ng serve` in the project directory. The application will be available at http://localhost:4200/.

## Examples
The following examples are available in this demo application:

* Signal Example
The signal example demonstrates how to use Angular 16 signals. A signal is a way to notify subscribers when an event occurs. In this example, a signal is used to notify the application when the user clicks a button. The button is defined as a standalone component.

* Standalone Component Example
The component example demonstrates how to use Angular 16 components standalone. In this example, a simple component is defined that displays a message. The component is then used in the main application component to display the message.

## Online Demo
https://angular-signals-demo.xilerth.com/