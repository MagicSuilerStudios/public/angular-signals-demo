import { Component, signal, effect, Injector, computed } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-signals-variables',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './signals-variables.component.html',
  styleUrls: ['./signals-variables.component.scss'],
})
export class SignalsVariablesComponent {
  public count = signal<number>(0);
  public power = computed(() => this.count() ** 2);
  public title = signal<string>('Hello World');

  constructor(private injector: Injector) {
    effect(
      () => {
        console.log(`count is ${this.count()}`);
      },
      { injector: this.injector }
    );

    effect(() => {
      console.log(`text is ${this.title()}`);
    });

    effect(() => {
      console.log(`count is ${this.count()} and text is ${this.title()}`);
    });
  }

  public increment(): void {
    this.count.update((count) => count + 1);
  }

  public decrement(): void {
    this.count.update((count) => count - 1);
  }

  public reset(): void {
    this.count.set(0);
  }

  public updateTitle(title: string): void {
    this.title.set(title);
  }
}
