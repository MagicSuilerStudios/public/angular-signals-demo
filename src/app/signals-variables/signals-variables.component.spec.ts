// Import necessary libraries
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SignalsVariablesComponent } from './signals-variables.component';

describe('SignalsVariablesComponent', () => {
  let component: SignalsVariablesComponent;
  let fixture: ComponentFixture<SignalsVariablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SignalsVariablesComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignalsVariablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('count should initialize to 0', () => {
    expect(component.count()).toBe(0);
  });

  it('should increment count', () => {
    component.increment();
    expect(component.count()).toBe(1);
  });

  it('should decrement count', () => {
    component.decrement();
    expect(component.count()).toBe(-1);
  });

  it('should reset count to 0', () => {
    component.reset();
    expect(component.count()).toBe(0);
  });
});
