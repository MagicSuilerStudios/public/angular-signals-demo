import {
  Component,
  ElementRef,
  ViewChild,
  ViewChildren,
  signal,
  effect,
  Injector,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoStorageService } from '../signal-list/todo-storage.service';
import { Task } from '../signal-list/task.interface';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-zone-list',
  standalone: true,
  imports: [CommonModule],
  providers: [TodoStorageService],
  templateUrl: './zone-list.component.html',
  styleUrls: ['./zone-list.component.scss'],
})
export class ZoneListComponent {
  //get input element from dom
  @ViewChild('todoName') todoName: ElementRef | undefined;
  @ViewChild('todoDescription') todoDescription: ElementRef | undefined;

  constructor(private todoStorageService: TodoStorageService) {
    this.todoList.subscribe((todoList) => {
      this.todoStorageService.save(todoList);
    });
  }

  public todoList: BehaviorSubject<Task[]> = new BehaviorSubject<Task[]>(
    this.todoStorageService.get()
  );

  public addToDoTask(): void {
    const id =
      new Date().getMilliseconds() + Math.floor(Math.random() * 100000);
    const toDoTask: Task = {
      id,
      title: this.todoName?.nativeElement.value,
      completed: false,
      date: new Date(),
      description: this.todoDescription?.nativeElement.value,
    };

    this.todoList.next([...this.todoList.value, toDoTask]);
  }

  public removeTask(id: number): void {
    this.todoList.next(
      this.todoList.value.filter((todo) => todo.id !== id)
    );
    
  }

  public sendEvent() {
    this.todoName?.nativeElement.focus();
  }

  public completeTask(id: number): void {
    this.todoList.next(
      this.todoList.value.map((todo) => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    );
  }
}
