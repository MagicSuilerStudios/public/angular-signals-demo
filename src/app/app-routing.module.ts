import { ZoneListComponent } from './zone-list/zone-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'signals-variables',
  },
  {
    path: 'todo-list',
    loadComponent: () =>
      import('./signal-list/signal-list.component').then(
        (m) => m.SignalListComponent
      ),
  },
  {
    path: 'zone-todo-list',
    loadComponent: () =>
      import('./zone-list/zone-list.component').then(
        (m) => m.ZoneListComponent
      ),
  },
  {
    path: 'signals-variables',
    loadComponent: () =>
      import('./signals-variables/signals-variables.component').then(
        (m) => m.SignalsVariablesComponent
      ),
  },
  { path: '**', redirectTo: 'signals-variables' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
